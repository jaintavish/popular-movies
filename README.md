# PopularMovies
The 3rd Project of Udacity's Nanodegree program
Stage 2 of the previously developed Popular Movies

It displays you the popular and top rated movies of the current time. You can see the ratings, release dates, synopsis, reviews and trailers of those movies
Also, you can add movies to favourites so that you can have a look at them later
